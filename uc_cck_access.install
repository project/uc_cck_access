<?php

/**
 * @file
 * uc_cck_access module install file.
 */

/**
 * Implements hook_schema().
 *
 * @return
 *   The schema which contains the structure for the uc_cart_links module's tables.
 */
function uc_cck_access_schema() {
  $schema['uc_cck_access_products'] = array(
    'fields' => array(
      'pfid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'model' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'delay_type' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
        'description' => 'Delay type for access start: immediate, period, date',
      ),
      'delay_period' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'delay_date' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'description' => '(CURRENTLY UNUSED)',
      ),
      'duration_type' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
        'description' => 'Duration type for access stop: indefinite, period, date',
      ),
      'duration_period' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'duration_date' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'handler' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'access_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'type' => 'varchar',
        'length' => 512,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('pfid'),
    'indexes' => array(
      'access_id' => array('access_id'),
      'model' => array('model'),
      'handler' => array('handler'),
    ),
  );

  $schema['uc_cck_access_delays'] = array(
    'fields' => array(
      'did' => array(
        'description' => 'The primary identifier for a delay.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'inception' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'duration_type' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
        'description' => 'Duration type for access stop: indefinite, period, date',
      ),
      'duration_period' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'duration_date' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'pfid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'handler' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'access_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'data' => array(
        'type' => 'varchar',
        'length' => 512,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('did'),
    'indexes' => array(
      'uid' => array('uid'),
      'pfid' => array('pfid'),
      'inception' => array('inception'),
    ),
  );

  $schema['uc_cck_access_expirations'] = array(
    'fields' => array(
      'eid' => array(
        'description' => 'The primary identifier for an expiration.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'expiration' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'pfid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'default' => 0,
      ),
      'handler' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'access_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'type' => 'varchar',
        'length' => 512,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('eid'),
    'indexes' => array(
      'uid' => array('uid'),
      'pfid' => array('pfid'),
      'access_id' => array('access_id'),
      'expiration' => array('expiration'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function uc_cck_access_install() {
  // The drupal_install_schema() function is called automatically in D7.
}

/**
 * Implements hook_uninstall().
 */
function uc_cck_access_uninstall() {
  // The drupal_uninstall_schema() function is called automatically in D7.
  db_delete('uc_product_features')
  ->condition('fid', 'cck_access')
  ->execute();
//  db_query("DELETE FROM {variable} WHERE name LIKE 'ucca_%%'");
  variable_del('ucca_default_handler');
  variable_del('ucca_date_format');
  variable_del('ucca_node_weight');
}

